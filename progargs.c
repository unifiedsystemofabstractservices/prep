
#ifdef PROGARGS

char * progargs ( unsigned int argc , char ** argv ) {

	char * r ;
	unsigned int argptr ;
	char * delim = "," ;
	char * oarg ;
	char * narg ;
	r = "" ;
	argptr = argc - 1 ;
	// concatenate all of the arguments (except the last), with a delimiter
	while ( argptr > 1 ) {
		if ( argptr > 2 ) { narg = concat ( argv [argptr] , delim ) ; }
		// don't delimit the next to last argument
		else { narg = concat( "" ,argv [argptr] ) ; }
		// reference the concatenation
		oarg = r ;
		r = concat ( r , narg ) ;
		// release the old args
		free ( oarg ) ;
		// and the new arg
		free ( narg ) ;
		argptr -= 1 ;
	} //while

	return r ;
} // function

#endif
