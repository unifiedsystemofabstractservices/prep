/*
* String vector routines for Utility Services of Abstract Systems.
*/

/*
* First known version: Jan-08-2019
*/

#include "textenc.h"

#ifndef STRING
#define STRING 1
#endif

#ifndef STRNCOMPARE
#define STRNCOMPARE
#include "STRING/strncompare.c"
#endif

#ifndef STRCOMPARE
#define STRCOMPARE
#include "STRING/strcompare.c"
#endif

#ifndef STRCONCAT
#define STRCONCAT
#include "STRING/strconcat.c"
#endif

#ifndef STRLENGTH
#define STRLENGTH
#include "STRING/strlength.c"
#endif

#ifndef STRIM
#define STRIM
#include "STRING/strim.c"
#endif

#ifndef STRLOCATE
#define STRLOCATE
#include "STRING/strlocate.c"
#endif

#ifndef STRTOKEN
#define STRTOKEN
#include "STRING/strtoken.c"
#endif

#ifndef STRCOPY
#define STRCOPY
#include "STRING/strcopy.c"
#endif


#ifndef STRFROMINT
#define STRFROMINT
#include "STRING/strfromint.c"
#endif

/*
 * STREOS  -  finds and returns the string termination position.
 */
#ifndef STREOS
#define STREOS
unsigned char * streos ( char * s ) {
	unsigned char * c = s ;
	while ( * c != EOS ) { c += 1 ; }
	return c ;
} // function
#endif


/*

#ifndef STRNCAT
#define STRNCAT 1
#include "STRING/strncat.c"
#endif

#ifndef STRNCMP
#define STRNCMP
#include "STRING/strncmp.c"
#endif

#ifndef STRCPY
#define STRCPY 1
#include "STRING/strcpy.c"
#endif

#ifndef STRCHR
#define STRCHR 1
#include "STRING/strchr.c"
#endif

#ifndef STRRCHR
#define STRRCHR 1
#include "STRING/strrchr.c"
#endif

*/

