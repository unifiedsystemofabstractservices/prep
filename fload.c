#include <stdio.h>
#include <stdlib.h>

#ifdef FLOAD
/* return the length of an allocation (thats pointed to), or NULL on error */
unsigned int fload ( void ** p , unsigned int offset , char * fname ) {

	unsigned int r = 0 ;

	unsigned int t ;
	// read a file in binary mode
	FILE * f = fopen ( fname , "rb" ) ;
	if ( f != NULL ) {
		// seek to the offset
		t = fseek ( f , offset , SEEK_SET ) ;
		// if seeking returned ERR-NONE
		if ( t == 0 ) {
			t = 1 ;
			// read to the EOF in byte intervals, discarding the byte read
			while ( t == 1 ) { t = fread ( & t , 1 , 1 , f ) ; }
			// get the length of the file stream, minus the beginning offset
			r = ftell ( f ) ;
			if ( r > 0 ) {
				// disregard the EOF in the stream
				r -= 1 ;
				r -= offset ;
					// get a new address to store the file contents
					* p = malloc ( r ) ;
					if ( p != NULL ) {
						// seek back to the offset
						fseek ( f , offset , SEEK_SET ) ;
						// populate the buffer one byte per read
						t = fread ( *p , 1 , r , f ) ;
						// if the number of bytes read are not the size of the buffer
						if ( t != r ) {
							free ( *p ) ;
							// store the address of null
							* p = NULL ;
							// return a length of zero
							r = 0 ;
						}
					} // if memory was allocated
				}// if bytes were read to the EOF
			} // else the file contains data
	fclose ( f ) ;
	} // if opening the file was successful
	return r ;
}

#endif
