
#ifndef VECT

#define VECT

typedef struct vector {
	unsigned int l ;
	void * v ;
} vect ;

typedef struct lblvector {
	unsigned int l ;
	void * v ;
	unsigned char * c ;
} lblvect ;

#endif


#ifndef VECT_LOC
#define VECT_LOC
vect * vect_loc ( unsigned int l , void * b ){
	vect * r = malloc ( sizeof ( vect ) ) ;
	r -> l = l ;
	r -> v = b ;
	return r ;
} // function
void vect_uloc ( vect * p ) { free ( p ) ; }
#endif

#ifndef VECT_REL
#define VECT_REL
void vect_rel ( vect * p ) {
	if ( p != NULL ) {
		if ( p -> v != NULL ) { free ( p -> v ) ; }
		free ( p ) ;
	}
}
#endif

#ifndef REVECT
#define REVECT
vect * revect( unsigned int l, void * b ) {
	vect * r = malloc ( sizeof ( vect ) ) ;
	r -> l = l ;
	// get a new buffer
	r -> v = malloc ( l ) ;
	unsigned char * dest = r -> v ;
	unsigned char * src = b ;
	// duplicate the data pointed to by (b) to the return value
	while ( l > 0 ) { l -= 1 ; dest [l] = src [l] ; }
	return r ;
} // function
#endif

#ifndef VECT_EXT
#define VECT_EXT
/*
 * vector extend - append a data buffer(b) to a vector(p); returning the concatenation
 */
vect * vect_ext ( vect * p , vect * b ) {
	vect * r = malloc ( sizeof ( vect ) ) ;
	r -> l = p -> l + b -> l ;
	r -> v = malloc( r -> l ) ;
	unsigned char * dest = ( ( unsigned char * ) r -> v ) ;
	unsigned char * src = ( ( unsigned char * ) p -> v ) ;
	unsigned int l = 0 ;
	// duplicate the first vector
	while ( l < p -> l ) { dest [l] = src [l] ; l += 1 ; }
	// append the second vector to the first
	dest += p -> l ;
	src = ( ( unsigned char * ) b -> v ) ;
	l = 0 ;
	while ( l < b -> l ) { dest [l] = src [l] ; l += 1 ; }
	return r ;
} // function
#endif

#ifndef VECT_ADD
#define VECT_ADD
/*
 * vector add - only pass vectors that have been re-vectored
 */
void vect_add ( vect ** v , unsigned char * s , unsigned int n ) {

	unsigned int t = 0 ;
	unsigned int vl = 0 ;
	unsigned char * vd ;

	if ( * v != NULL ) {
		unsigned char * d = ( * v ) -> v ;
		vl = ( * v ) -> l ;
		// manually re-vector, derived from the length
		vd = malloc ( vl + n ) ;
		while ( t < vl ) {
			vd [t] = d [t] ;
			t += 1 ;
		} // while

		vect_rel ( * v ) ;

	} else { vd = malloc ( n ) ; }

	vl += n ;

	while ( n > 0 ) {
		vd [t] = * s ;
		s += 1 ;
		t += 1 ;
		n -= 1 ;
	} // while
	// reference the allocation that has just populated with the concatenation
	* v = vect_loc ( vl , vd ) ;

} // function
#endif
