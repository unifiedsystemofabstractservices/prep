
#ifdef DEQPUSH

/*
 * PUSH - add an item to the end of the list
 */

void deqpush ( void * i  , deque ** q ) {
	// initialize the list with the item
	if ( deqchk ( * q ) == NULL ) { * q = deq_loc ( i ) ; }
	else {
	// save a reference to the current item
	qent * ci = ( * q ) -> item ;
	// seek to the end of the list
	DEQLAST ( * q )
	// create a new item
	qent * ni = qent_loc ( i , ( * q ) -> item , NULL ) ;
	// add a reference of the item to the list end
	( * q ) -> item -> link -> next = ni ;
	// restore the direction to the item
	( * q ) -> item = ci ;
	( * q ) -> length += 1 ;
	} // else
} // function


/*
 * RPUSH - add an item to the beginning of the list
 */

void deqrpush ( void * i  , deque ** q ) {
	// initialize the list with the item
	if ( deqchk ( * q ) == NULL ) { * q = deq_loc ( i ) ; }
	else {
	// save a reference to the current item
	qent  * ci = ( * q ) -> item ;
	// seek to the beginning of the list
	DEQFIRST ( * q )
	// create a new item
	qent * ni = qent_loc ( i , NULL , ( * q ) -> item ) ;
	// add a reference of the item to the list
	( * q ) -> item -> link -> prev = ni ;
	// restore the direction to the item
	( * q ) -> item = ci ;
	( * q ) -> length += 1 ;
	} // else
} // function

#endif
