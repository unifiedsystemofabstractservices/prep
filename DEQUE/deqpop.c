
#ifdef DEQPOP

void deqpop ( deque * q ) {

	if ( q != NULL ) {

		if ( q -> item != NULL ) {

			qent * prev = NULL ;

			qent * next = NULL ;

			qent * point = NULL ;
			// patch the queue links around the current item
			if ( q -> item -> link -> prev != NULL ) {
				prev = q -> item -> link -> prev ;
			}
			if ( q -> item -> link -> next != NULL ) {
				next = q -> item -> link -> next ;
				point = next ;
				if ( prev != NULL ) {
					prev -> link -> next = next ;
					next -> link -> prev = prev ;
				} else { next -> link -> prev = NULL ; }
			} else if ( prev != NULL ) {
				prev -> link -> next = NULL ;
				//reference the previous item if the next is unavailable
				point = prev ;
			} // else the next item is null, but the previous is not

			q -> item -> rel ( q -> item ) ;

			q -> item = point ;

			q -> length -= 1 ;

		} // if the item is valid

	} // if the parameter is valid

} // function

#endif
