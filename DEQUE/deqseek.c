/*
 * Move the item pointed to in the queue (n) places forward
 */

#ifdef DEQSEEK

void deqseek ( deque * l , unsigned int n ) {

//seek the list's current position
while ( n > 0 ) {
	if ( l -> item -> link -> next != NULL ) {
		l -> item = l -> item -> link -> next ;
	}
	n -= 1 ;
} // while

} // function

/*
 * Move the item pointed to in the queue (n) places in reverse
 */
void deqrseek ( deque * l , unsigned int n ) {

while ( n > 0 ) {
	if ( l -> item -> link -> prev != NULL ) {
		l -> item = l -> item -> link -> prev ;
	}
	n -= 1 ;
} // while

} // function

#endif
