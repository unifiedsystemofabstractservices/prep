
#ifdef DEQINSERT

/*
 * LSTINSERT - add an item after the one currently pointed at
 */

void deqinsert ( void * i , deque ** q ) {
	if ( deqchk ( * q ) == NULL ) { * q = deq_loc ( i ) ; }
	else {
		qent * ni = qent_loc ( i , ( * q ) -> item , ( * q ) -> item -> link -> next ) ;
		// link the next item to the new item
		 if ( ( * q ) -> item -> link -> next != NULL ) {
		 ( ( qent * ) ( * q ) -> item -> link -> next ) -> link -> prev = ni ;
		 }
		 // link the current item to the new item
		 ( * q ) -> item -> link -> next = ni ;
		 ( * q ) -> length += 1 ;
	} // else
} // function


/*
 * LSTRINSERT - insert an item prior to the one currently pointed at
 */

void deqrinsert ( void * i , deque ** q ) {
	if ( deqchk ( *q ) == NULL ) { * q = deq_loc ( i ) ; }
	else {
		qent * ni = qent_loc ( i , ( * q ) -> item -> link -> prev , ( * q ) -> item ) ;
		// link the prev item to the new item
		 if ( ( * q ) -> item -> link -> prev != NULL ) {
		 ( ( qent * ) ( * q ) -> item -> link -> prev ) -> link -> next = ni ;
		 }
		 // link the current item to the new item
		 ( * q ) -> item -> link -> prev = ni ;
		 ( * q ) -> length += 1 ;
	} // else
} // function

#endif
