/*
 * PREPENVAR - Inserts preprocessor variables in a text buffer.
 *
 * Accepts a buffer as a labeled vector and returns a new buffer.
 *
 */

#ifdef PREP

// avoid host system redefinition
enum predefs { DEF_STDC , DEF_FILE , DEF_LINE , DEF_DATE , DEF_TIME } ;

unsigned char * prep_predef [] = { "__STDC__" , "__FILE__" , "__LINE__" , "__DATE__" , "__TIME__" } ;

vect * prepenvar ( lblvect * b ) {
	unsigned char * prep_preval [] = {"0","","","0","0"};

	unsigned char * sym = b -> v ;
	unsigned char * symmark = sym ;
	unsigned char * endsym = b -> v ;
	endsym += b -> l ;
	unsigned int line = 1 ;
	unsigned char envar ;

	vect * r = vect_loc ( 0 , NULL ) ;
	vect * pr ;
	vect * t ;

	// update the preval filename, and line
	prep_preval [DEF_FILE] = b -> c ;
	prep_preval [DEF_LINE] = strfromint ( line ) ;

	while ( sym != endsym ) {
		if ( * sym == EOL ) {
			// release the old string
			free ( prep_preval [DEF_LINE] ) ;
			line += 1 ;
			prep_preval [DEF_LINE] = strfromint ( line ) ;
		}
		// loop the predef envars
		envar = 0 ;
		while ( envar < 5 ) {
			// peek a couple of symbols
			if ( * sym == * prep_predef [envar] ) {
				if ( sym [1] == prep_predef [envar] [1] ) {

					// pull the next token from the buffer to a local array
					unsigned char termpos = strlength ( prep_predef [envar] ) ;
					unsigned char term [termpos + 1] ;
					term [termpos] = EOS ;
					while ( termpos > 0 ) { termpos -= 1 ; term [termpos] = sym [termpos] ; }

					// jump to a string processing function
					if ( strcompare ( term , prep_predef [envar] ) == 0 ) {
					unsigned int termlen = strlength ( prep_preval [envar] ) ;
					// reference the current buffer state
					pr = r ;
				// copy the length of the term, and symbols prior to the last mark
					t = revect ( ( sym - symmark ) + termlen , symmark ) ;
					// add previous symbols to a new buffer
					r = vect_ext ( r , t ) ;
					// release the old buffer state, but not the pointer
					vect_rel ( pr ) ;
					// release the memory duplicated between the two marks
					vect_rel ( t ) ;
					// add the symbols of the term
					termlen += 1 ;
					while ( termlen > 0 ) {
					termlen -= 1 ;
	( (char*) ( r -> v ) ) [(r->l) - termlen] = ( prep_preval [envar] ) [termpos] ;
					// value was exhausted before strcompare
					termpos += 1 ;
					} // while
					// update the read pos. and advance past the envar
					symmark = sym + strlength ( prep_predef [envar] ) ;
					// anticipate the loop end increment
					sym = symmark - 1 ;
					// stop searching for envars
					envar = 4 ;
					} // if an environment variable is found
				}
			}
			envar += 1 ;
		} // nested while searching the predef envar strings
		sym += 1 ;
	} // while the end of the stream is not encountered


	// add any concluding symbols; by pointing to the original buffer
	t = vect_loc ( sym - symmark , symmark ) ;
	pr = r;
	r = vect_ext ( r , t ) ;
	// release the attributes of the general pointers
	vect_rel ( pr ) ;
	vect_uloc ( t ) ;

	return r ;

} // function

#endif
