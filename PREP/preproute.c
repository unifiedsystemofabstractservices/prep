
/*
 * SUBROUTE - substitute the parameter list (p) into the routine definition (f)
 *  Return the body of the expanded function.
 */
#ifdef PREP

/*
 * Extract parameters from the string (p)
 */
vect * extparam ( unsigned char * p ) {
	vect * r = NULL ;
	// duplicate the parameter list and remove the encapsulating parenthesis
		unsigned int plen = strlength( p ) ;
		unsigned char * beg = nsearch ( LPAREN , p , plen ) ;
		unsigned char * end = nsearch ( RPAREN , p , plen ) ;
		if ( beg != NULL && end != NULL ) {
			// advance past the point of the symbol
			beg += 1 ;
			plen = (unsigned int)end - (unsigned int)beg ;
			unsigned char * c = loc ( plen + 1 ) ;
			c [plen] = EOS ;
			MEMCOPY ( c , beg , plen )
			// parse the parameter list into tokens
			unsigned char divgraph [] = { COMMA , EOS } ;
			r = tokenize ( c , divgraph ) ;
			// trim tokens
			unsigned int toknum = r -> l ;
			unsigned char ** tok;
			while ( toknum > 0 ) {
				toknum -= 1 ;
				tok = (unsigned char **) r -> v ;
				tok[toknum] = strim ( tok[toknum] ) ;
			} // while
		}
	return r ;
} // function

/*
 * Release resources possibly allocated by extparam
 */
void relparam ( vect * p ) {
	if ( p != NULL ) {
		rel ( ((unsigned char **) (p -> v)) [0] ) ;
		rel ( p -> v ) ;
		rel ( p ) ;
	}
}

unsigned char * preproute ( unsigned char * p , unsigned char * f ) {

	unsigned char * r = EOS ;
		// tokenize the substitutional parameters
		vect * subtokens = extparam ( p ) ;
		unsigned char ** sub = ( unsigned char ** ) ( subtokens -> v ) ;
		// extract parameters from the function definition
		vect * hosttokens = extparam ( f ) ;
		unsigned char ** host = ( unsigned char ** ) ( hosttokens -> v ) ;

		// if both strings have parameters
		if ( subtokens != NULL ) {
			if ( hosttokens != NULL ) {
				// hold the lengths of the functions parameters
				unsigned int toknum = hosttokens -> l ;
				unsigned int hostlen [toknum];
				while ( toknum > 0 ) {
					toknum -= 1 ;
					hostlen [toknum] = strlength ( host [toknum]) ;
				} // while
				// hold the lengths of the substitutional parameters
				toknum = subtokens -> l ;
				unsigned int sublen [toknum] ;
				while ( toknum > 0 ) {
					toknum -= 1 ;
					sublen[toknum] = strlength ( sub [toknum] ) ;
				} // while
				// scan the host function; requires a terminated string arg.
				unsigned int blocklen = strlength ( f ) ;
				// represents the symbol currently pointed to and parsed
				unsigned int count = 0 ;
				// seek past the parameter list of the host function
				unsigned char * c = f ;
				while ( * c != RPAREN ) {
					c += 1 ;
					count += 1 ;
				} // while
				// skip the symbol previously sought after
				c += 1 ;
				count += 1 ;
				// hold bounding markers of the parsing position
				unsigned char * pbeg = c ;
				unsigned char * pend = c ;
				vect * duproute = NULL ;
				unsigned int duplen = 0 ;
				// point to every symbol of the function body
				while ( count < blocklen ) {
					// compare each symbol of the host with its params
					unsigned int hostnum = 0 ;
					while ( hostnum < hosttokens -> l ) {
						if ( strncompare ( c , host [hostnum] , hostlen [hostnum] ) == 0 ) {
							// point to the symbol after a matching param
							unsigned char * postsym = c + hostlen[hostnum] ;
					// eliminate false-positives by testing for allowable variable symbols
							if ( ! isalph ( * postsym ) && ! isalph ( * ( c - 1 ) ) ) {
								// don't count the symbol that triggered this catch
								if ( pend != pbeg ) { duplen -= 1 ; }
								// update the last scanned symbol point
								pend = c ;
								//copy intermediate symbols between the markers
								vect_add ( & duproute , pbeg , duplen ) ;
								if ( hostnum < subtokens -> l ) {
									//inject the corresponding substitute parameter
									vect_add ( & duproute , sub [hostnum] , sublen [hostnum] ) ;
								} else {
		// a parameter index of the host definition extends the length of the substitutions
									unsigned char * proxy = strfromint ( NULL ) ;
									vect_add ( & duproute , proxy , strlength ( proxy ) ) ;
									rel ( proxy ) ;
								}
								// reset the symbol count to duplicate
								duplen = 0 ;
								// hold a reference to the parameters end
								pbeg = postsym ;
							} // if a following symbol is not permissible in variables id's
						} // if a host parameter is found in its body
						hostnum += 1 ;
					} // while each host parameter is iterated
					duplen += 1 ;
					c += 1 ;
					count += 1 ;
				} // while
				/* if there has been one substitution and the length
 	 	 	 	 * was incremented the same iteration that it was reset
				 */
				if ( pend != pbeg ) { duplen -= 1 ; }
				// copy any trailing symbols that are not parameters
				vect_add ( & duproute , pbeg , duplen ) ;
				// copy-out the modified routine to the return string
				duplen = duproute -> l ;
				c = duproute -> v ;
				r = loc ( duplen + 1 ) ;
				MEMCOPY ( r , c , duplen )
				// terminate the return string
				r [duplen] = EOS ;
				// release tokenized-parameter assets
				relparam(hosttokens);
			}
			relparam(subtokens);
		}

	return r ;

} // function

#endif
