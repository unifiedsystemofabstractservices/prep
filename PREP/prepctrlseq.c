
#ifndef CASCADE
enum cascade { SEL , SKIP , ALT , ALTSKIP , END , ENDSEL } ;
#define CASCADE
#endif

#ifdef PREP

// toggles between true and false states (depends on bool)
unsigned char in_cascade ;
unsigned char alt_selectable ;
unsigned int alt_depth ;
//skip accumulator, state toggle count-down
unsigned int nest_count ;

unsigned char pr ;

// accessor to this files definition
unsigned char cascading ( void ) { return in_cascade ; }

void prepctrlreset ( void ) {
	// set the cascade state to true, and reset the skip counter
	in_cascade = true ;
	alt_selectable = false ;
	alt_depth = 0 ;
	nest_count = 0 ;
	pr = SEL ;
}

unsigned char prepctrlseq ( unsigned char c ) {

	unsigned char r = SKIP ;

	unsigned char nest_par ( void ) {
		if ( alt_depth > 0 && alt_depth == nest_count ) { return true ; }
		else { return false ; }
	}

	void rtn_end ( void ) {
		if ( pr != END ) { r = END ; }
		else { r = SKIP ; }
	}

	if ( c == SEL ) {
		if ( in_cascade == true ) {
			// start another selection block after closing the last
			if ( nest_count > 0 && pr == END ) { r = SEL ; }
			else { r = ENDSEL ; }
		} else { rtn_end () ; }
		// when cascading and not, allow an alternate selection in another nest
		nest_count += 1 ;
	} else 	if ( c == ALT ) {
		if ( in_cascade == true ) {
			//end the cascade
			in_cascade = false ;

			if ( nest_par () == true ) {
				//puts("ending the chain");
				alt_depth += 1 ;
			}
			//puts("here");
			rtn_end () ;
		} else {
			if ( alt_selectable == true ) {
				in_cascade = true ;
				alt_selectable = false ;
				// skipping always ends, so alternates always select
				r = SEL ;
				//puts("ALT-CHAIN");
			} // if skipping a selection has enabled alt-selecting
		} // else not in cascade, and an alternate is selectable
	} else if ( c == SKIP ) {
		if ( in_cascade == true ) {
			// toggle both states
			in_cascade = false ;
			alt_selectable = true ;

			//puts("ending the chain from not selecting");
			// close the last open selection block on the first SEL
			rtn_end () ;
			// escalate the chance of an alternate when going out of cascade
			alt_depth += 1 ;

		}
		// track the number of nests, even when not in cascade
		nest_count += 1 ;

	} else if ( c == ALTSKIP ) {
		if ( in_cascade == true ) {
			//end the cascade
			in_cascade = false ;

			//puts("ending the chain");
			// keep the members intact, but message to end any selection
			rtn_end () ;
		}
		// fall-through with skipping when not in cascade
	} // alt-skip


	if ( c == END ) {
		if ( nest_count > 0 ) {
			// decrement the skip count if possible
			nest_count -= 1 ;

			if ( nest_count == 0 ) {
				// reopen on the last end, after an end
				if ( pr == END ) { r = SEL ; }
				// close the last block if still in cascade
				else { r = ENDSEL ; }
				// total reset when a nesting ends, prepare for another round
				prepctrlreset();
			} // after a controlled end

			else {
				// multiple ends are encountered while in cascade
				if ( in_cascade == true ) {
					if ( pr != END ) { r = ENDSEL ; }
					else { r = SEL ; }
				} else {
					if ( pr != END ) { r = END ; }
					else {
						//printf("nest: %i , alts: %i\n", nest_count, alt_depth);
						if ( alt_depth > 0 && (alt_depth-1) == nest_count ) {
							in_cascade = true ;
							alt_selectable = false ;
							r = SEL ;
						} // the end of a false selection without an alternate
						// else r falls-through as SKIP
					}
				}
			}

		} // if the nest count has not exhausted
	} // if an END is passed

	/*
	if ( in_cascade == true ) {
		puts("\n---cascade is true---");
	}else{
		puts("\n---cascade is false---");
	} // if the sequence is not skipped

	//printf("Nest count: %i\n",nest_count);
	//printf("ALT depth: %i\n", alt_depth);
	*/

	// capture the last non-skipped return
	if ( r != SKIP ) pr = r ;

	return r ;

} // function

#endif
