/*
 * Expands (#) macros in the parameter
 */

#ifdef PREP

void prepenhance ( unsigned char * cmd ) {

	unsigned char * subptr = cmd ;
	// scan the command for a sub-flag
	while ( * subptr != EOS ) {
		if ( * subptr == NUM ) {
			// accumulate the number of flags
			unsigned char subcnt = 0 ;
			while ( * subptr == NUM ) { subcnt += 1 ; subptr += 1 ;}
			// parse sub-directive macros
			if ( subcnt == 1 ) {
				/*
				 * The index points to the character after the flag, and
				 * quotes are delimited by the next space or the buffers end.
				 */
				subptr [-1] = QUOTE ;
				while ( * subptr != WSPACE ) {
					// truncate the string
					if ( subptr [1] == EOS ) { * subptr = WSPACE ; }
					else { subptr += 1 ; }
				} // while
				* subptr = QUOTE ;
			} if ( subcnt == 2 ) {
				/*
				 * Offset two pointers by the difference of the flag length,
				 * and copy back the prefixing values until the delimiter, or
				 * beginning of the buffer is read.
				 */
				// point back to the last flag symbol
				subptr -= 1 ;
				// skip prefixing sub-directive flags
				char * concatptr = subptr;
				while ( * concatptr == NUM ) { concatptr -= 1 ; }
				// find the previous delimiter: space or the buffers beginning
				char catend = 0 ;
				while (catend == 0 ) {
					if ( * concatptr == WSPACE ) { catend = 1 ; }
					else {
					// copy a preceding value to the start of the concat.
					* subptr = * concatptr ;
					if ( concatptr == cmd ) { catend = 1 ; }
					else {
						subptr -= 1 ;
						concatptr -= 1 ;
					} // else keep adjusting the pointers and don't terminate
					} // else the buffer is not pre-delimited
				} // while
				// overwrite characters between the difference of the pointers
				while ( concatptr < subptr ) {
					* concatptr = WSPACE ;
					concatptr += 1 ;
				} // while patching the pointer difference
			} // flag switch
		} // if a flag is found
		subptr += 1 ;
	} // while

} // function

#endif
