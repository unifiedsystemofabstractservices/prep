
/*
 * PREPEXTRACT - copies an excerpt of characters from a buffer to another;
 * 			guided by precluding and successive key sequences, with escape.
 *
 * Accepts p - buffer to read from,
 * b - buffer to extract to,
 * and s - three pointers to string keys.
 *
 * Return the last read position of the input buffer (p)
 */

#ifdef PREP

unsigned char * prepextract ( vect * p , vect * b , unsigned char ** s ) {

	unsigned char * readptr = p -> v ;

	unsigned char * buffend = readptr + p -> l ;

	char * presym = s [0] ;

	char * postsym = s [1] ;

	char * escsym ;

	// assign the escape sequence an unused non-printable
	if ( * s [2] == NULL ) { * escsym = ESC ; }
	else { escsym = s [2] ; }

	char * presymptr = presym ;

	char * postsymptr = postsym ;

	char * postescptr = escsym ;

	char * excerptptr = b -> v ;

	char * excerptend = excerptptr + b -> l ;
	// terminate the beginning of the output buffer
	* excerptptr = EOS ;

	void * expoint = NULL ;

	unsigned int exlen = 0 ;

	char exit = 0 ;

	while ( exit == 0 ) {
	if ( * presymptr != EOS ) {
		// if one symbol matches
		if ( * readptr == * presymptr ) {
			presymptr += 1 ;
			if ( * presymptr == EOS ) {
				// save the length of the pre-symbol sequence
				// presymptr points to a control char, so sub. one
				exlen = presymptr - presym - 1 ;
				// mark the point of extraction
				expoint = readptr - exlen ;
			} // if a pre-sequence has been parsed
		} else {
			// reset the sequence when the symbol does not match
			presymptr = presym ;
			if ( * readptr == * presymptr ) { presymptr += 1 ; }
		}
	} else {
		if ( * postsymptr == EOS ) { exit = 1 ; }
		else {
			exlen += 1 ;
			// scope tab 1
			if ( * postescptr == EOS ) {
				// reset the post-sequence pointer to the beginning
				postsymptr = postsym ;
				// rewind the excerpt buffer write position
				while ( postescptr != escsym ) {
					// backtrack the buffer pointer
					excerptptr -= 1 ;
					// also reset the escape pointer
					postescptr -= 1 ;
				} //while
				// skip all symbols to the sentinel (line continuation)
				char lineflag = 0 ;
				while ( lineflag == 0 ) {
					if ( readptr < buffend ) {
						if ( * readptr != EOL ) { readptr += 1 ; exlen += 1 ; }
						else { lineflag = 1 ; }
					} else { lineflag = 1 ; }
				} // while
			} else {
				// scope tab 2
				if ( * readptr == * postescptr ) { postescptr += 1 ; }
				else { postescptr = escsym ; }
				// if a post directive symbol will be read
				if ( * readptr == * postsymptr ) { postsymptr += 1 ; }
				else {
					// reset the post-sequence pointer
					postsymptr = postsym ;
					// if space remains in the output buffer
					if ( excerptptr != excerptend ) {
					// store the line in a buffer
					* excerptptr = * readptr ;
					excerptptr += 1 ;
					} else {
						// terminate the start of the output buffer
						* ( ( char *) b -> v ) = EOS ;
						// flag for break
						exit = 1 ;
					} // else the directive buffer is full
				} // else the post sequence is not being read
				// scope tab 2
			} // else line termination is not escaped
			//scope tab 1
		} // else the pre-sequence is not being parsed
	} // else a directive is being parsed
	// test if more data can be parsed
	if ( readptr != buffend ) { readptr += 1 ; }
	else { exit = 1 ; }
	} // while
	//terminate the buffer
	* excerptptr = EOS ;
// after terminating the output buffer,point to the excerpted input buffer
	excerptptr = expoint ;
	// overwrite the buffer length of the extracted term
	while ( exlen > 0 ) {
		* excerptptr = WSPACE ;
		excerptptr += 1 ;
		exlen -= 1 ;
	} // while

	// return the last parsed position
	if ( readptr != p -> v ) { readptr -= 1 ; }

	return readptr ;

} // function

#endif
