/*
* ASCII mnemonics
*/

#ifndef NULL
#include "null.h"
#endif

#ifndef ASCII

#define ASCII

/* non-printable */
#define EOS NULL
#define BACK 8
#define HZTAB 9
#define EOL 10
#define VIRTAB 11
#define ESC 27
// national range
#define DELETE 127

#define WSPACE 32
#define EXCL 33 // !
#define QUOTE 34 // "
#define NUM 35 // #
#define CASH 36 // $
#define PCT 37 // %
#define AMP 38 // &
#define APOS 39 // '
#define LPAREN 40 // (
#define RPAREN 41 // )
#define AST 42 // *
#define PLUS 43 // +
#define COMMA 44 // ,
#define SIGN 45 // -
#define POINT 46 // .
#define FSLASH 47 // /
#define NUM_OFFSET 48 // 0
// numbers range
#define COLON 58
#define SCOLON 59
#define LT 60
#define EQ 61
#define GT 62
#define QUEST 63
#define ADDR 64 // @
// upper letters
#define LBRACE 91 // [
#define BSLASH 92
#define RBRACE 93 // ]
#define CAROT 94 // ^
#define USCORE // _
#define ACCT // '
// lower letters
#define LBRACKET 123 // {
#define PIPE 124 // |
#define RBRACKET 125 // }
/* symbolizes NOT; reciprocal */
#define TILDE 126 // ~

#endif
