/*
 * DCOMMENT - Removes single and multi-line C style comments from a buffer (b),
 * 			(l) bytes in length.
 */

#ifdef DCOMMENT

unsigned char * cmtsym [2] [4] = { { FSLASH , FSLASH  , EOL , NULL } , { FSLASH , AST , AST , FSLASH } } ;

void dcomment ( unsigned int l , void * b ) {
	unsigned int len = l ;
	char * sym ;
	/* holds the index of the next comment symbol to compare, or zero */
	char cproc ;
	char blank ;
	char none = 0 ;
	char single = 1 ;
	char multi = 2 ;
	char either = 3 ;
	char cstate ;
	cstate = none ;
	blank = 0 ;
	cproc = 0 ;
	sym = ( char * ) b ;

	while ( len > 0 ) {
		if ( cproc == 0 ) {
			blank = 0 ;
			if ( cstate == either ) {
				if ( *sym == cmtsym [0] [1] ) { cstate = single ; }
				else if ( *sym == cmtsym [1] [1] ) { cstate = multi ; }
				else { cstate = none ; }
				// if a digraph was caught
				if ( cstate != none ) {
					// enable comment processing
					cproc = 1 ;
					// overwrite this and last iterations symbols
					blank = 1 ;
					*( sym - 1 ) = WSPACE ;
				}
			} else {
				if ( *sym == cmtsym [0] [0] ) { cstate = either ; }
			} // else cstate is none
		} else {
			if ( *sym == cmtsym [cstate - 1] [cproc + 1] ) {
				if ( cstate == single ) {
					// disable comment processing
					cproc = 0 ;
					cstate = none ;
					// don't overwrite the delimiter
					blank = 0 ;
				}
				else {
					if ( cproc == 2 ) { cproc = 0 ; cstate = none ; }
					else { cproc += 1 ; }
				} //else processing a multi-line comment
			} else {
				// if the first symbol of the termination digraph matches
				if ( *sym == cmtsym [cstate - 1] [2] ) { cproc = 2 ; }
				// revert
				else { cproc = 1 ; }
			} // else the digraph doesn't match
		} // else processing a comment
		// comments are treated as white-space
		if ( blank != 0 ) { *sym = WSPACE ; }
		sym += 1 ;
		len -= 1 ;
	} // while

} // function

#endif
