/*
* Universal type NULL, null, for Utility Services of Abstract Systems.
*/

#ifdef NULL
#undef NULL
#endif

#ifndef NULL
#define NULL 0
#endif

#ifdef null
#undef null
#endif

#ifndef null
#define null NULL
#endif
