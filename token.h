
#include "textenc.h"


#ifndef XMEM
#define XMEM
#include "xmem.c"
#endif

/* used by tokenize */
#ifndef STRTOKEN
#define STRTOKEN
#include "STRING/strtoken.c"
#endif

/* also used by tokenize */
#ifndef VECT
#include "vect.h"
#endif

#ifndef TOKEN
#define TOKEN
#endif

#ifndef NUMTOKENS
#define NUMTOKENS
#include "TOKEN/numtokens.c"
#endif

#ifndef TOKENAT
#define TOKENAT
#include "TOKEN/tokenat.c"
#endif

#ifndef TOKENIZE
#define TOKENIZE
#include "TOKEN/tokenize.c"
#endif

#ifndef UNTOKEN
#define UNTOKEN
#include "TOKEN/untoken.c"
#endif
