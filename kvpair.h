
#ifndef KVPAIR

#define KVPAIR

typedef struct kvpair_struct {
	unsigned char * key ;
	unsigned char * val ;
	void ( * rel ) ( void * ) ;
} kvpair ;

void kvpair_rel ( void * kv ) {
rel ( ( ( kvpair * ) kv ) -> val ) ;
rel ( ( ( kvpair * ) kv ) -> key ) ;
rel ( kv ) ;
}

kvpair * kvpair_loc () {
	kvpair * r = loc ( sizeof( kvpair ) ) ;
	r -> rel = & kvpair_rel ;
	return r ;
}

#endif
