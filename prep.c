
#include "textenc.h"

#include "bool.h"

#ifdef PREP

// TODO: causes crazy errors outside the PREP definition

#ifndef XMEM
#define XMEM
#include "xmem.c"
#endif

/* required by prepenvar */
#ifndef VECT
#include "vect.h"
#endif

/* structure of prep_defs -> item -> data */
#ifndef KVPAIR
#include "kvpair.h"
#endif

#ifndef DEQUE
#include "deque.h"
#endif

#ifndef STRING
#include "string.h"
#endif

#ifndef TOKEN
#include "token.h"
#endif

#ifndef MEMOPS
#include "memops.h"
#endif

#ifndef NEXTPRIME
#define NEXTPRIME
#include "nextprime.c"
#endif

/* program specific routines */
#include "PREP/prepenvar.c"
#include "PREP/prepenhance.c"
#include "PREP/prepextract.c"
#include "PREP/prepctrlseq.c"
#include "PREP/preproute.c"

#ifndef CASCADE
enum cascade { SEL , SKIP , ALT , ALTSKIP , END , ENDSEL } ;
#define CASCADE
#endif

/* used in un/define commands which removes a definition, which it is pointed to, from the queue; freeing all associated pointers */
#define PREP_UDEF(term) if ( prep_defined ( term ) != false ) { kvpair* pair = prep_defs -> item -> data ; deqpop ( prep_defs ) ; pair -> rel ( pair ) ; }


/* VARIOUS GRAPHS */
// functional expansion
unsigned char expgraph [] = { LPAREN , EOS } ;
unsigned char wspgraph [] = { WSPACE , EOS } ;
// definition sequences
unsigned char prep_presym[] = { EOL , NUM , EOS } ;
unsigned char prep_postsym[] = { EOL , EOS } ;
unsigned char prep_postesc[] = { BSLASH ,EOS } ;
// package the symbol sequences; can be a 2D array
unsigned char * prep_symseq [3] = { prep_presym , prep_postsym , prep_postesc } ;

unsigned char * prep_term [10] = {"include","define","undef","defined","ifdef","ifndef","endif","else","if","elif"};
enum terms { INCLUDE , DEFINE , UNDEF , DEFINED , IFDEF , IFNDEF ,ENDIF, ELSE , IF , ELIF } ;


/* GLOBAL */

vect * prep_fvect ;

unsigned char * prep_err ;

// PATH directory forwarded from the program parameter
unsigned char * prep_path ;
// extracted directive line(s)
unsigned char * prep_cmd ;
// arbitrary buffer size
unsigned char prep_cmdlen = 255 ;
// package for cmdlen and cmd
vect * prep_cmdvect ;

unsigned char * prep_fname ;

// last read position of a file buffer when extracting definitions
unsigned char * prep_buffpos ;
// generated value for unspecified definitions
unsigned int prep_defval ;
// FIFO list of file name strings
deque * prep_includes;
// dictionary of key-value definitions
deque * prep_defs ;

deque * nest_prept ;
deque * nest_postpt ;

/*
 * Return the boolean result of a definition within a queue of key-value pares.
 */

unsigned char prep_defined ( unsigned char * c ) {

	unsigned char r = false ;

	if ( prep_defs != NULL ) {
		// search in reverse, as new entries are pushed to the queues end,
		// and it is likely that queries occur in the same domain
		DEQLAST ( prep_defs )
		// reference the last queue item
		qent * def = prep_defs -> item ;
unsigned char ** t = ( unsigned char ** ) ( ( kvpair * ) prep_defs -> item ) -> key ;
		while ( t != NULL ) {
			if ( strcompare ( * t , c ) == 0 ) {
				// set the return to true
				r = true ;
				// save a reference to the matching item
				def = prep_defs -> item ;
				//  break the loop
				t = NULL ;
			} // if the parameter has been matched to a key
			// break the loop or rewind the list position one link
			if ( prep_defs -> item -> link -> prev == NULL ) { t = NULL ; }
			else {
			DEQRBUMP ( prep_defs ) ;
			t = ( unsigned char ** )( ( kvpair * ) prep_defs -> item ) -> key ;
			}
		} // while parsing the queue
	// set the queue to the matching item, or the last item
	prep_defs -> item = def ;
	} // if the definition list has been initialized

	return r ;

} // function

/*
 * Interpret the format of the command buffer, and forward execution logic.
 */

void prep_direct( void ) {

	unsigned char nest_state = SKIP ;

	unsigned int cmdlen = strlength ( prep_cmd ) + 1 ;

	unsigned char * cmdcopy = loc ( cmdlen ) ;

	MEMCOPY ( cmdcopy , prep_cmd , cmdlen )

	vect * cmdtokens = tokenize ( cmdcopy , wspgraph ) ;

	unsigned char ** token = ( unsigned char ** ) ( cmdtokens -> v ) ;

	if ( cmdtokens -> l > 1 ) {
	if ( cascading () == true ) {

	if ( strcompare ( token [0] , prep_term[DEFINE] ) == 0 ) {
		kvpair * newdef = kvpair_loc ( ) ;
		unsigned char * termval ;
		unsigned char * termkey = token [1] ;
		// release the definition pointed to by the queue if it exists
		PREP_UDEF(termkey)
		// check if the term contains a functional expansion graph
		unsigned char * defend = strlocate ( termkey , expgraph ) ;
		// forward the parameter expansion process to value substitution
		if ( * defend != EOS ) {
			// revert the token operation
			untoken ( cmdtokens , WSPACE ) ;
			// extract the function definition label
			unsigned int lbllen = 0 ;
			while ( defend != termkey ) {
				lbllen += 1 ;
				defend -= 1 ;
			}
			newdef -> key = loc ( lbllen + 1 ) ;
			MEMCOPY ( (newdef -> key) , termkey , lbllen )
			//terminate the label copy
			(newdef -> key) [lbllen] = EOS ;
			//overwrite the function label with terminators
			while ( lbllen > 0 ) {
				lbllen -= 1 ;
				* termkey = EOS ;
				termkey += 1 ;
			} // while
			// reference a mid-string point as the value
			termval = termkey ;
		} else {
			unsigned int keylen = strlength ( termkey ) + 1 ;
			newdef -> key = loc ( keylen ) ;
			MEMCOPY ( (newdef -> key) , termkey , keylen )
			// if a value was provided for the definition, point to the string
			if ( cmdtokens -> l > 2 ){
				termval = token [2] ;
			} else {
				prep_defval = nextprime ( prep_defval ) ;
				termval = strfromint ( prep_defval ) ;
			} // else generate a value, and convert it to a string
		} // else a functional expansion symbol was not found
		// trim the key
		newdef -> key = strim ( newdef -> key ) ;
		// trim the value
		termval = strim ( termval ) ;
		unsigned int vallen = strlength ( termval ) + 1 ;
		newdef -> val = loc ( vallen ) ;
		MEMCOPY ( (newdef -> val) , termval , vallen )
		// terminate the value string
		( newdef -> val ) [vallen] = EOS ;
		// append to the definitions list
		deqpush ( newdef , & prep_defs ) ;
	} else if ( strcompare ( token [0] , prep_term[UNDEF] ) == 0 ) {
		// the command has already been tested for more than one term
		PREP_UDEF( token [1] )
	} else if ( strcompare ( token [0] , prep_term[INCLUDE] ) == 0 ) {
		// heads up: file names cannot contain token delimiters
		unsigned char * phrase = token [1];
		// copy and skip the first symbol;
		char cap = * phrase ;
		phrase += 1 ;
		// write a terminator into the token vector over reserved symbols
		unsigned char * t = phrase ;
		while ( * t != EOS ) {
			// if a cap is found, overwrite it and break
			if ( * t == cap ) { * t = EOS ; }
			else if ( * t == GT ) { * t = EOS ; }
			else { t += 1 ; }
		} // while
	 //if the parameter has been traversed and does not immediately terminate
		if ( t != phrase ) {
			unsigned int filelen = strlength ( phrase ) ;
			unsigned char * filename ;
			// if the PATH specifier symbol is used
			if ( cap == LT ) {
				unsigned int pathlen = strlength ( prep_path ) ;
				filelen += pathlen ;
				filename = loc ( filelen + 1 ) ;
				MEMCOPY ( filename , prep_path , pathlen )
				// after copying PATH, copy the file name pointed to by t
				filelen -= pathlen ;
				t = filename + pathlen ;
				// don't copy the string cap
				MEMCOPY ( t , phrase , filelen )
			} else {
				// copy the filename verbatim
				filename = loc ( filelen + 1 ) ;
				MEMCOPY( filename , phrase , filelen )
			} // else including a relative file
			//terminate the file name string
			filename [filelen] = EOS ;
			// add a new file URL to the inclusion queue
			deqpush ( filename , &prep_includes ) ;
		} // if a terminator was injected
	} // if the INCLUDE directive is encountered
	} // if in cascade

	if ( strcompare ( token [0], prep_term[IFNDEF] ) == 0 ) {
		// use the inverse procedure as defined
		if ( prep_defined ( token [1] ) == false ) { nest_state = prepctrlseq ( SEL ) ; }
		else { nest_state = prepctrlseq ( SKIP ) ; }
	} else if ( strcompare ( token [0] , prep_term[IFDEF] ) == 0 ) {
		// if the next token exists in the defined queue
		if ( prep_defined ( token [1] ) != false ) { nest_state = prepctrlseq ( SEL ) ; }
		else { nest_state = prepctrlseq ( SKIP ) ; }
	} else if ( strcompare ( token [0] , prep_term[IF] ) == 0 ) {
		if ( cmdtokens -> l > 2 ) {
			if ( strcompare ( token [1] , prep_term[DEFINED] ) == 0 ){
				if ( prep_defined ( token [2] ) != false ) { nest_state = prepctrlseq ( SEL ) ; }
				else { nest_state = prepctrlseq ( SKIP ) ; }
			} // ifdef long hand
		} // if there are three or more tokens
	} else if ( strcompare ( token [0] , prep_term[ELIF] ) == 0 ) {
		if ( cmdtokens -> l > 2 ) {
			if ( strcompare ( token [1] , prep_term[DEFINED] ) == 0 ){
				if ( prep_defined ( token [2] ) != false ) { nest_state = prepctrlseq ( ALT ) ; }
				else { nest_state = prepctrlseq ( ALTSKIP ) ; }
			}
		}
	} // elif term
	} // if there is more than one token
	else if ( strcompare ( token [0] , prep_term[ELSE] ) == 0 ) {
		nest_state = prepctrlseq ( ALT ) ;
	} else if ( strcompare ( token [0] , prep_term[ENDIF] ) == 0 ) {
		nest_state = prepctrlseq ( END ) ;
	}
// release the command duplicate, and pointers to its tokens
rel ( cmdcopy ) ;
rel ( cmdtokens -> v ) ;
rel ( cmdtokens ) ;


/*
switch(nest_state){
case(SKIP):
		puts(" skip");
break;
case(SEL):
		puts(" select");
break;
case(END):
		puts(" end");
break;
case(ENDSEL):
		puts(" re-start");
break;
}
*/

if ( nest_state == END || nest_state == ENDSEL ) {
	if ( deqchk ( nest_prept ) != NULL && deqchk ( nest_postpt ) != NULL ) {
		// seek to the end of the parallel queues
		DEQLAST ( nest_prept )
		DEQLAST ( nest_postpt )
		// point to the last non-terminated queue
		qent * entry = nest_postpt -> item ;
		if ( entry -> data == NULL ) {
			entry -> data = prep_buffpos ;
			// write the current point to a start entry when an end entry is completed
			if ( nest_state == ENDSEL ) { nest_state = SEL ; }
		} // if a null entry was found in the post-point queue
	} // if both queues are initialized
} // END nest state

if ( nest_state == SEL ) {
	// add new entries to both queues, and point to the entries
	deqpush ( prep_buffpos , & nest_prept ) ;
	DEQBUMP ( nest_prept )
	deqpush ( NULL , & nest_postpt ) ;
	DEQBUMP ( nest_postpt )
}

} // function

/*
 * Get the next file name to include
 */

void prep_fnext ( unsigned char ** s ) {

	if ( prep_fname != NULL ) { rel ( prep_fname ) ; prep_fname = NULL ; }

	if ( prep_includes != NULL ) {
		if ( prep_includes -> item != NULL ) {
			// copy the current file name string
			unsigned char * s = prep_includes -> item -> data ;
			unsigned int l = strlength ( s ) ;
			prep_fname = loc ( l + 1 ) ;
			MEMCOPY ( prep_fname , s , l )
			prep_fname [l] = EOS ;
			// this frees the string allocated by the include directive
			// and points to the next item, if any
			deqpop ( prep_includes ) ;
			// if the next item is null, the one previously bump'd to in 'pop
			if ( prep_includes -> item == NULL ) {
				// release the queue and reference null
				deq_rel ( prep_includes ) ;
				prep_includes = NULL ;
			}
		}
	} // if there are file names in the queue to include

	* s = prep_fname ;

} // function


/*
 * Return the name of the next file to load after parsing the parameters buffer
 */

unsigned char * prep_ifile ( lblvect * b ) {
	// clear previous instances of the rebuild buffer
	if ( prep_fvect != NULL ) { vect_rel ( prep_fvect ) ; prep_fvect = NULL ; }

	// relabel environment variables in the buffer, returning a new buffer
	prep_fvect = prepenvar ( b ) ;
	unsigned char * fbuff = prep_fvect -> v ;
	unsigned int bufflen = prep_fvect -> l ;

	// add the first entries to the nest resolution queues
	deqpush ( fbuff , & nest_prept ) ;
	deqpush ( NULL , & nest_postpt ) ;
	// reset the cascade sequence controller
	prepctrlreset ( ) ;

	/*
	 * Find directives based on the parameters of prep_sysmseq
	 */

	prep_buffpos = prepextract ( prep_fvect , prep_cmdvect , prep_symseq ) ;
	// interpret a parsed directive
	while ( * prep_cmd != EOS ) {
		// trim the command of white space
		prep_cmd = strim ( prep_cmd ) ;
		prep_direct() ;
		// fetch another command if the director does not throw an error
		if ( prep_err != EOS ) { * prep_cmd = EOS ; }
		else {
		prep_buffpos = prepextract ( prep_fvect , prep_cmdvect , prep_symseq ) ;
		}
	} // while



	if ( nest_postpt -> item -> data != NULL ) {
		prep_err = "sequence corruption" ;
	}

	if ( prep_err == EOS ) {

		/*
		 * Create a duplicate buffer from the sequence bound queues
		 */

		// add buff-end to the nest_post end point
		nest_postpt -> item -> data = fbuff + bufflen ;
		// rewind both nest queues
		DEQFIRST ( nest_prept )
		DEQFIRST ( nest_postpt )
		// rebuild the file using the nest members
		unsigned char * end = nest_postpt -> item -> data ;
		unsigned char * start = nest_prept -> item -> data ;
		// start the sequence with the first nest range
		vect * reseq = revect ( ( ( unsigned int ) end - ( unsigned int ) start ) , start );
		// reference the number of start points as the number of sequences to bind
		unsigned int seqnum = nest_prept -> length - 1 ;
		while ( seqnum > 0 ) {
				DEQBUMP ( nest_prept )
				DEQBUMP ( nest_postpt )
				end = nest_postpt -> item -> data ;
				start = nest_prept -> item -> data ;
				// separate the nest scopes end and beginning symbols by injecting a space
				vect_add ( & reseq , wspgraph , strlength( wspgraph ) ) ;
				// concatenate the next start onto the last end
				vect_add ( & reseq , start , ( ( unsigned int ) end - ( unsigned int ) start )) ;
				seqnum -= 1 ;
		} // while
		// release the queue of pointers to the duplicate file buffer
		deq_rel ( nest_prept ) ;
		deq_rel ( nest_postpt ) ;
		nest_prept = NULL ;
		nest_postpt = NULL ;

		/*
		 * Duplicate the sequenced buffer, replacing definitions and macros
		 */

		DEQFIRST ( prep_defs )
		kvpair * defkv ;
		unsigned int numdefs = 0 ;
		if ( deqchk ( prep_defs ) != NULL ){ numdefs = prep_defs -> length ; }
		unsigned int defnum = 0 ;
		// store the length of the definitions for quicker repetitive iteration
		unsigned int deflen [numdefs] ;
		while ( defnum < numdefs ) {
			// quick cast
			defkv = prep_defs -> item -> data ;
			deflen [defnum] = strlength ( defkv -> key ) ;
			DEQBUMP ( prep_defs )
			defnum += 1 ;
		} //while
		DEQFIRST ( prep_defs )
		defnum = 0 ;

		vect * reline = NULL ;
		vect * rebuff = NULL ;

		void restack ( void ) {
			// load the vector into a terminated string
			unsigned int len = reline -> l ;
			unsigned char * s = loc ( len + 1 ) ;
			unsigned char * ss = reline -> v ;
			MEMCOPY ( s , ss , len )
			s [len] = EOS ;
			// parse the string for macros
			prepenhance ( s ) ;
			// dump the string to the buffer
			vect_add ( & rebuff , s , len ) ;
			// release the temporary string
			rel ( s ) ;
			// reset the line vector
			vect_rel ( reline ) ;
			reline = NULL ;
		} // nested function

		void restring ( unsigned int l , unsigned char * s ) {
		// copy the line to an unterminated string that begins and ends with a space
			unsigned int len = l + 2 ;
			unsigned char * insrt = loc ( len ) ;
			insrt += 1 ;
			MEMCOPY ( insrt , s , l )
			insrt -= 1 ;
			insrt [0] = WSPACE ;
			insrt [len-1] = WSPACE ;
			// duplicate string to the line buffer
			vect_add ( & reline , insrt , len ) ;
			//release the string
			rel ( insrt ) ;
		} // nested function

		unsigned int duplen = 0 ;
		// test every symbol of the sequence for a definition match
		unsigned char * c = reseq -> v ;
		end = reseq -> v + reseq -> l ;
		while ( c != end ) {
			if ( * c != EOL ) {
				if ( * c != WSPACE ) {
					while ( defnum < numdefs ) {
						defkv = prep_defs -> item -> data ;
						if ( strncompare ( c , defkv -> key , deflen [defnum] ) == 0 ) {
							if ( ! isalph ( * ( c - 1 ) ) && ! isalph ( * ( c + deflen [defnum] ) ) ) {
							// string any symbols prior to the definition, after ENDL
							if ( duplen > 0 ) { restring ( duplen , c - duplen ) ; }
							// search one symbol of the definition value for the
							// parameter list bounding cap using nsearch from memops.h
							//unsigned char * plist = nsearch ( LPAREN , defkv -> val , 1 ) ;
							//if ( plist != NULL ) {
							if ( (defkv -> val) [0] == LPAREN ) {
								unsigned int plen = 0 ;
								// skip the definition key
								c += deflen [defnum] ;
								// find the length of the calls parameter list
								while ( * c != RPAREN ) { plen += 1 ; c += 1 ; }
								c -= plen ;
								// copy the list
								unsigned char * params = loc ( plen + 1 ) ;
								MEMCOPY ( params , c , plen + 1 )
						// the symbol pointer (c) is incremented past RPAREN at the end of the loop
								c += plen ;
								params [plen+1] = EOS ;
								// in case there were spaces between the definition key and parameters,
								// trim the parameter list before using it
								unsigned char * route = strim ( preproute ( strim ( params ) , defkv -> val ) ) ;
								// add the definition value to the line
								restring ( strlength ( route ) , route ) ;
								//release temporary assets
								rel ( route ) ;
								rel ( params ) ;
							} else {
								// add the definition value to the line
								restring ( strlength ( defkv -> val ) , defkv -> val ) ;
								//skip the definition label-key
								c += ( deflen [defnum] - 1 ) ;
							}// else a parameter list starting cap is not found in the definition
							// reset the length of symbols to duplicate
							duplen = 0 ;
							// break the loop
							defnum = numdefs ;
							} // if false positives are eliminated
						} // if a point in the sequence matches a definition key
						DEQBUMP ( prep_defs )
						defnum += 1 ;
					} // nested while searching the definitions
					/*
					 * When a definition is found the counter is set to match the sentinel
					 * which is then surpassed; otherwise the counter will match.
					 */
					// the symbol is not the start of a definition key
					if ( defnum == numdefs ) { duplen += 1 ; }
					// reset the definition searching members
					defnum = 0 ;
					DEQFIRST ( prep_defs )
				} // if the symbol is not a space
				// if the symbol is a space, but follows a non-space after ENDL
				else if ( duplen > 0 ) { duplen += 1 ; }
			} else {
// add any symbols, including those following a definition if non-space symbol/s were parsed
				if ( duplen > 0 ) { restring ( duplen , c - duplen ) ; }
				if ( reline != NULL ) { restack ( ) ; }
				// reset the symbol length to duplicate
				duplen = 0 ;
			} // else ENDL is found
			c += 1 ;
		} // while
		// if the buffer ended without an ENDL after a string
		if ( duplen > 0 ) { restring ( duplen , c - duplen ) ; }
		if ( reline != NULL ) { restack ( ) ; }
		// release the re-sequenced buffer source
		vect_rel ( reseq ) ;
		// release the file buffer duplicate that was returned by prepenvar
		// the original file buffer is released by the caller
		if ( prep_fvect != NULL ) { vect_rel ( prep_fvect ) ; prep_fvect = NULL ; }
		// if populated, writable lines in the buffer were parsed
		if ( rebuff != NULL ) {
			// get a new container and reference the rebuilt vector
			prep_fvect = vect_loc ( rebuff -> l , rebuff -> v ) ;
		}

	} // if errors were not thrown while directing

	/*
			unsigned int e = prep_fvect -> l ;
			unsigned int ee = 0 ;
			while(ee<e){
				putchar(((char*)(prep_fvect -> v))[ee]);
				ee++;
			}
	*/
		/*
			if (prep_defs != NULL ){
				unsigned int l = 0;
				DEQFIRST(prep_defs)

				while ( l < prep_defs->length) {
					puts(((kvpair*)(prep_defs->item->data))->key);
					DEQBUMP(prep_defs)
					l++;
				}
			}
		*/

} // function



/* forward the number of bytes to write, and the buffer */
void prep_ofile ( vect * v ) {
	if ( prep_err == EOS && prep_fvect != NULL ) {
		v -> l = prep_fvect -> l ;
		v -> v = prep_fvect -> v ;
	} else { v = NULL ; }
} // function

void prep_fini ( void ) {
	// release all the general use pointers except prep_err
	rel ( prep_cmd ) ;
	rel ( prep_cmdvect ) ;
	deq_rel ( prep_defs ) ;
	deq_rel ( prep_includes ) ;
	if ( prep_fvect != NULL ) { vect_rel ( prep_fvect ) ; }
}

void prep_init ( unsigned char * c ) {
	prep_path = c ;
	// in the future, zalloc
	prep_cmd = loc ( prep_cmdlen ) ;
	// create a command-container object which references prep_cmd
	prep_cmdvect = vect_loc ( prep_cmdlen , prep_cmd ) ;

	prep_includes = NULL ;
	prep_defs = NULL ;
	// set the first prime value
	prep_defval = 3 ;

	nest_prept = NULL ;
	nest_postpt = NULL ;
	prep_fvect = NULL ;
	prep_fname = NULL ;
	// points to the location of the EOS symbol, or another string
	prep_err = EOS ;
}

#endif
