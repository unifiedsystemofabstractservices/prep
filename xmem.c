
#ifdef XMEM

void * loc ( unsigned int s ) {
	return malloc ( s ) ;
}

void rel ( void * s ) {
	free ( s ) ;
}

#endif
