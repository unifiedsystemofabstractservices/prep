
#ifndef MEMOPS
#define MEMOPS
unsigned int memn ;
unsigned char memt ;
#endif


/* MEMCOPY - Copy (n) values from one address from another, byte by byte.
 *
 * Accepts two pointers and an integer. Returns nothing.
 */

#ifndef MEMCOPY
#define MEMCOPY(s,ss,n) memn = n ; while ( memn > 0 ) { * s = * ss ; s += 1; ss += 1 ; memn -= 1 ; } s -= n ; ss -= n ;
#endif


/*
 * MEMSET - Copy the value of (c) beginning at the address (s), (n) times.
 *
 * Accepts two pointers and an integer. Returns nothing.
 */

#ifndef MEMSET
#define MEMSET(s,c,n) memn = n ; while ( memn > 0 ) { * s = * c ; s += 1 ; memn -= 1 ; } s -= n ;
#endif


/*
 * MEMSWAP - Exchange (n) bytes between (s) and (ss).
 *
 * Accepts two pointers and an integer. Returns nothing.
 */

#ifndef MEMSWAP
#define MEMSWAP(s,ss,n) memn = n ; while ( memn > 0 ) { memt = * s ; * s = * ss ; * ss = memt ; s += 1 ; ss += 1 ; memn -= 1 ; } s -= n ; ss -= n ;
#endif


#ifndef NSEARCH
#define NSEARCH
void * nsearch ( unsigned char b , unsigned char * s , unsigned int n ) {
	void * r = NULL ;
	memn = n ;
	while ( memn > 0 ) {
		if ( * s == b ) {
			r = s ;
			memn = 0 ;
		} else {
			s += 1 ;
			memn -= 1 ;
		}
	} // while
	return r ;
} // function
#endif


