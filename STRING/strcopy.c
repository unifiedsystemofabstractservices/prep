/**********************************************************************
*   STRCOPY  -  copies one string over another up to the length of the host.
*
*	BYTE *strcpy(s1,s2)
*	BYTE *s1, *s2;
*
*	Returns an overwritten string with termination.
*
*
***********************************************************************/

#ifdef STRCOPY

char * strcopy ( char * s , char * ss ) {

char * c = s ;

while( * ss != EOS ) {
	if ( * c != EOS ) {
		//copy one char per iteration
		* c = * ss ;
		//advance the pointers of both strings
		c += 1 ;
	} //if the letter within the first string is not a terminator
	ss += 1 ;
} //while
//append a terminator to the first parameter reference
* c = EOS ;

return s ;

} //function

#endif
