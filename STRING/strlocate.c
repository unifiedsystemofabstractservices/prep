
/*
 * STRLOCATE - Version of strindex. Find string (ss) in string (s).
 *
 * Return the position of the found string, or the terminator.
 */

#ifdef STRLOCATE

unsigned char * strlocate ( unsigned char * s , unsigned char * ss ) {

	if ( * s == EOS )  return s ;
	if ( * ss == EOS ) return ss ;

	unsigned char exit = 0 ;
	unsigned char * ptnstart = ss ;
	unsigned int ptnlen = 0 ;
	unsigned char * strend = s;
	while ( * strend != EOS ) { strend += 1 ; }
	while ( * ss != EOS ) { ss += 1 ; ptnlen += 1 ; }
	ss -= ptnlen ;

	while ( !exit ) {
		// break prematurely
		if ( * s == EOS || * ss == EOS ) { exit = 1 ; }
		else {
			if ( * ss == * s ) { ss += 1 ; }
			else {
				// reset the pointer of the second string
				ss = ptnstart ;
				// check for a match of the first byte
				//to para-lize the upcoming string advancement
				if ( * ss == * s ) { ss += 1 ;}
			}
			// prime for next iteration
			s += 1 ;
		} // else the first string has not terminated
	} // while

	// if the loop was broken by a sequence match, not an exhausted string
	if ( *ss == EOS ) return s -= ptnlen  ;
	else return strend ;

} // function

#endif
