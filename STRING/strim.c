/*
 * STRIM - Remove leading and trailing white space from the parameter.
 *
 * Return the adjusted, re-terminated parameter in the same vector.
 */

#ifdef STRIM

unsigned char * strim ( unsigned char * s ) {

	unsigned char * c = s ;

	unsigned char * ss = s ;

	// find the first non-white space symbol
	while ( * s == WSPACE ) { s += 1 ; }

	// copy all post leading symbols to the start of the string
	while ( * s != EOS ) {
		// copy the value to itself when the pointers match
		* ss = * s ;
		ss += 1 ;
		s += 1 ;
	} // while
	// point to before EOS when there are no beginning spaces
	if ( * ss == EOL ) { ss -= 1 ; }
	if ( * ss == WSPACE ) {
		// find the last non-white space symbol
		while ( * ss == WSPACE ) { ss -= 1 ; }
		// reference the first white space after the string
		ss += 1 ;
	}
	// nullify the difference between the last symbol and the EOS
	while ( * ss != EOS ) { * ss = EOS ; ss += 1 ; }

	return c ;

} // function

#endif
