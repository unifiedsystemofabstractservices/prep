/**********************************************************************
*   STRLENGTH  -  counts the number of letters in a string vector.
*
*	WORD strlen(s)
*	BYTE *s;
*
*	Returns the number of characters preceeding a terminator.
*
**********************************************************************/

#ifdef STRLENGTH

unsigned int strlength ( unsigned char * s ) {

unsigned int n = 0 ;

/* advance until a terminator is referenced. */
while ( * s != EOS ) { s += 1 ; n += 1 ; }

return n ;

} //function

#endif
