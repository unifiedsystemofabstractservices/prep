/**********************************************************************
*   STRNCPY  -  copies at most (n) chars from one string over another.
*
*	BYTE *strcpy(s1,s2,n)
*	BYTE *s1, *s2;
*	UWORD n;
*
*	Copies at most n bytes from s2 to s1, stopping after null has been moved.
*	Does not check for overflow of s1.
*	Returns s1.
*
***********************************************************************/

#ifdef STRNCPY

char * strncpy ( char * string_one , char * string_two , int num_letters ) {

char * cp ;

cp = string_one ;

while ( num_letters > 0 ) {
	if ( string_two [0] != EOS ) {
		if ( cp [0] != EOS ) {
			//duplicate one char to the first string
			cp [0] = string_two [0] ;
			//advance both string pointers
			*cp += 1 ;
			*string_two += 1 ;
			//decrement the loop condition
			num_letters -= 1 ;
		} //if a terminator is not reached within the first string
		else { num_letters = 0 ; }
	} //if the second string pointer has not reached a terminator
	else { num_letters = 0 ; }
} //while
//terminate the first string
cp [0] = EOS ;

return string_one ;
} //function

#endif
