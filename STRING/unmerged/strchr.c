/*********************************************************************
*   STRCHR -  returns a pointer to first occurrence of char in string.
*	(formerly known as "index()")
*
*	BYTE *strchr(s,c)
*	BYTE *s, c;
*
*	Returns pointer to first c in s.
*	Post pass this functions return with streos to strcmp to determine
*	if the result overflowed; if ( (strchr - streos) == ENONE ) overflow
*
**********************************************************************/

#ifdef STRCHR

char * strchr ( char * string , char letter ) {
while ( string [0] != letter ) {
	if ( string [0] != EOS ) {
	//advance the string pointer
	*string += 1 ;
	} //if the string terminator is not encountered
	//break gracefully
	else { letter = string [0] ; }
} //while

// found the letter, note the 'index'
return string ;
} //function

#endif
