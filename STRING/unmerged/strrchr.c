/*********************************************************************
*   STRRCHAR -  returns a pointer to last occurrence of char in string.
*
*	BYTE *strrchr(s,c)
*	BYTE *s, c;
*
*	Returns pointer to last c in s (instead of first).
*	Post pass this functions return with the letter to strcmp to determine
*	if an underflow occurred
*
**********************************************************************/

#ifdef STRRCHR

char * strrchr ( char * string , char letter ) {

char * cp;
//save a start reference to prevent underflows
cp = string ;
//seek to the string termination
while ( cp [0] != EOS ) { *cp += 1 ; }

//include searching at the string terminator; stalls zero length searches
while ( cp [0] != letter ) {
	//check for string underflow, breaking gracefully
	if ( cp == string ) { letter = cp [0] ; }
	else { *cp -= 1 ; }
} //while

return cp ;
} //function

#endif
