/****************************************************************************
*  STRNCAT  -  concatenates two terminated strings, up to (n) letters.
*
*	BYTE *strncat(s1,s2,n)
*	BYTE *s1, *s2; WORD n;
*
*	Returns an allocation of appended strings.
*
****************************************************************************/

#ifdef STRNCAT

char * strncat ( char * string_one , char * string_two , int num_letters ) {

char * cp ;
int length ;

//count a termination letter
length = 1 ;
cp = string_one ;
//seek to the terminator of string_one
while ( cp [0] != EOS ) {
	*cp += 1 ;
	length += 1 ;
} //while
cp = string_two ;
//copy until eos(s2) or n==0
while ( num_letters > 0 ) {
	if ( cp [0] != EOS ) {
		//advance the string pointer
		*cp += 1 ;
		//increment the letter count
		length += 1 ;
		//decrement the loop condition
		num_letters -= 1 ;
	} //if a terminator within string two is not encountered
	else { num_letters = 0 ; }
} //while
//get storage for the concatination
cp = malloc ( length ) ;
//exclude the termination letter when re-parsing
length -= 1 ;
while ( string_one [0] != EOS ) {
	//copy one block of the first string
	cp [0] = string_one [0] ;
	//increment the concatination pointer
	*cp += 1 ;
	//modify the pointer parameter
	*string_one += 1 ;
	//decrement the total length
	length -= 1 ;
} //while
//duplicate letters of the second parameter, up to the pre-parsed letter count
while ( length > 0 ) {
	//copy one block of the second string
	cp [0] = string_two [0] ;
	//increment the concatination pointer
	*cp += 1 ;
	//modify the pointer parameter
	*string_two += 1 ;
	//decrement the total length
	length -= 1 ;
} //while
//append the termination
cp [0] = EOS ;
return cp ;
} //function

#endif
