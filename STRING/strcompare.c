/*********************************************************************
*   STRCOMPARE  -  compares null terminated byte vectors in parallel.
*	
*	Returns a number relative to the parameter with greater value, or zero.
*		example:
*		strcmp = 2  if  s1<s2, 
*		strcmp = 0  if  s1=s2,
*		strcmp = 1  if  s1>s2, 
*
*********************************************************************/

#ifdef STRCOMPARE

unsigned int strcompare ( unsigned char * s , unsigned char * ss ) {

unsigned int r ;

r = 3 ;

while ( r == 3 ) {
	//check the first block of both parameters for a terminator
	if ( * s != EOS ) {
		if ( * ss != EOS ) {
	//assign the flag a value that coinsides with the greater parameter
			if ( * s > * ss ) { r = 1 ; }
			else {
				if ( * s < * ss ) { r = 2 ; }
				else {
					s += 1 ;
					ss += 1 ;
				} //else advance both parameters
			} //else string_one is not greater than string_two
		} //if the second string is not terminated
		//if the first string is not terminated, and the second string is
		else { r = 1 ; }
	} //if the first string is not terminated
	else {
		//if the first string is terminated, and the second string is not
		if (  * ss != EOS ) { r = 2 ; }
		//the second string is also terminated
		else { r = 0 ; }
	} //else
} //while


return r ;

} //function

#endif
