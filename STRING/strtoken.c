/*
 * STRTOKEN - Accepts two strings, a primary string in which to find tokens,
 * 			and a delimiter string of which tokens are created.
 * 			This function replaces the in-line delimiter string with null, and
 * 			returns the first occurrence of a token, or null if none are found.
 *
 */

#ifdef STRTOKEN

char * strtoken ( char * s , char * d ) {

	char * c = s ;

	char * dptr = d ;

	while ( * c != EOS ) {
			if ( * c == * dptr ) { dptr += 1 ; }
			else {
				dptr = d ;
				// if a char of the string matches the first delimiter symbol
				if ( * c == * dptr ) { dptr += 1 ; }
			} // else the string symbols do not match
			// if a token has been found
			if ( * dptr == EOS ) {
				// overwrite the string the length of the delimiter string
				while ( dptr != d ) {
					* c = EOS ;
					c -= 1 ;
					dptr -= 1 ;
				} // while
			} //else { c += 1 ; }
			c += 1 ;
	} // while

	return s ;

} // function

#endif
