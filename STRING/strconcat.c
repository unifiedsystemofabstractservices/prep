/****************************************************************************
*    STRCONCAT  -  appends one string to another.
*
*	BYTE *strcat(s1,s2)
*	BYTE *s1, *s2;
*
*	Returns concatenated strings; assumes null terminated strings.
*
*
****************************************************************************/

#ifdef STRCONCAT

char * strconcat ( char * s , char * ss ) {

char * p ;

// reference the address of s1
p = s ;
// while the reference is not pointed at the termination
while ( * p != EOS ) { p += 1 ; }

while ( * ss != EOS ) {
	// duplicate one block of the first string
	* p = * ss ;
	// adjust both string pointers, or use p-f-n
	p += 1 ;
	ss += 1 ;
} // while

// append the terminator
* p = EOS ;

return p ;

} // function

#endif
