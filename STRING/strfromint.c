
#ifdef STRFROMINT

unsigned char * strfromint ( unsigned int i ) {

char buf_length = 15 ;

char length = 1 ;

char ptr = 1 ;

/* return that points to the c string value */
unsigned char * rtn ;

/* initialise a buffer to store the parsed value, 16 blocks in length,
the last block is reserved for EOL */
char * buf = malloc ( buf_length + 1 ) ;
/* write a zero to the first string digit position, for incrementing */
* buf = 0 ;

void digit_update () {
	/* while the next position is at the limit of the base*/
	while ( buf [ ptr ] == 9 ) {
		/* write a zero to the next counting position */
		buf [ ptr ] = 0 ;
		/* test the buffer length before advancing */
			if ( ptr < buf_length ) {
			ptr += 1 ;
			} //loop nested if
	} //while
	/* increment the greatest value that is less than the (base value(-1)) */
	buf [ ptr ] += 1 ;
	/* add the ones digit to the counter before updating the length */
	ptr += 1 ;
	/* update the greatest known length with a real number */
	if ( length <  ptr ) { length = ptr ; }
	/* reset the temporary digit position */
	ptr = 1 ;
} // nested function

/* size limited by the length of a register */
while ( i >= 2 ) {
/* if the buffer does not overflow */
if( length <= buf_length ) {
	/* if the base will overflow a digit position */
	if ( * buf == 8 ) {
		digit_update () ;
		/* write a zero to the ones' counting position */
		* buf = 0 ;
	/* otherwise continue counting by two */
	} else { * buf += 2 ; }
} //parent buffer condition
/* subtract two from the binary machine */
i -= 2;
} //while

/* account for a remain one value */
if ( i > 0 ) {
	if ( * buf < 9 ) {
	/* add to the string number */
	* buf += 1 ;
	}
	else {
		/* test the buffer before counting */
		if ( length <= buf_length ) {
		digit_update ( ) ;
		* buf = 0 ;
		}
	} //else
} //if there is a trailing one in the digital value

/* reset the temp. position for buffer copy */
ptr = 0 ;

/* if a null placeholder is not being used or there has been a base succession */
if ( length > 1 || * buf != 0 ) {
	/* allocate a substantial amount of memory for the return value */
	rtn = malloc ( length ) ;
	/* reverse digit subscript */
	char rds = 0 ;
		while ( ptr < length ) {
		/* equivalent to ( length - ( ptr + 1 ) ) */
		rds = length ;
		rds -= ptr ;
		rds -- ;
		/* reverse the order of the numbers */
		rtn [ ptr ] = buf [ rds ] ;
		/* transform the values to string values by adding the offset */
		rtn [ ptr ] += NUM_OFFSET ;
		/* setup for the next subscript */
		ptr ++ ;
		} //while
} else {
	/* one block for zero, and one to terminate the string */
	rtn = malloc ( 2 ) ;
	/* the acsii number offset points to the number zero */
	* rtn = NUM_OFFSET ;
}
/* as a subscript, the real number value (length) points the line terminator block, already */
/* mark the end of the string */
rtn [ length ] = EOS ;

/* cleanup resources */
free ( buf ) ;

return rtn ;

} //function

#endif
