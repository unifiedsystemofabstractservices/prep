/*********************************************************************
*   STRNCOMPARE  -  compares terminated string vectors up to (n) chars.
*
*	example:
*		strncmp = 2  if  s1<s2
*		strncmp = 0  if  s1=s2
*		strncmp = 1  if  s1>s2
*
*	Returns a number relative to the parameter with greater value, or zero.
*
*********************************************************************/

#ifdef STRNCOMPARE

unsigned int strncompare ( unsigned char * s , unsigned char * ss , unsigned int num_letters ) {

unsigned int r = 0 ;

while ( num_letters > 0 ) {
		if ( * s != EOS ) {
			if ( * ss != EOS ) {
				if ( * s > * ss ) { r = 1 ; num_letters = 0 ; }
				else if ( * ss > * s ) { r = 2 ; num_letters = 0 ; }
				else {
				//advance the pointers of both strings
				s += 1 ;
				ss += 1 ;
				//decrement the loop counter
				num_letters -= 1 ;
				}
			} else {
				r = 1 ;
				num_letters = 0 ;
			}//else, string two has terminated, but string one has not
		} else { 
			//if string one has terminated, but string two has not
			if ( * ss != EOS ) { r = 2 ; }
			// otherwise return flag as 0
			num_letters = 0 ;
		} //else
} //while
//flag remains unchanged if the num_letters is exhausted
return r ;
} //function

#endif
