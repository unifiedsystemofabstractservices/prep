
#include "textenc.h"

#include "bool.h"

#ifndef STRING
#include "string.h"
#endif

#ifndef XMEM
#define XMEM
#include "xmem.c"
#endif
/* used by preproute */
#ifndef MEMOPS
#include "memops.h"
#endif

#ifndef VECT
#include "vect.h"
#endif

#ifndef DEQUE
#include "deque.h"
#endif

#ifndef PREP
#define PREP
#include "prep.c"
extern unsigned char * prep_err ;
#endif


/*
#ifndef PROGARGS
#define PROGARGS 1
#include "progargs.c"
#endif
*/


#ifndef FLOAD
#define FLOAD
#include "fload.c"
#endif

#ifndef FSTORE
#define FSTORE
#include "fstore.c"
#endif

#ifndef DCOMMENT
#define DCOMMENT
#include "dcomment.c"
#endif

#include <stdio.h>


unsigned char * extend_str ( unsigned char * s , unsigned char * ss ) {
	unsigned int len = strlength ( s ) ;
	unsigned int relen = strlength ( ss ) + 1 ;
	unsigned char * r = loc ( len + relen  ) ;
	// copy the first parameter without a terminator
	MEMCOPY ( r , s , len )
	// copy the concatenation with a terminator
	r += len ;
	MEMCOPY ( r , ss , relen )
	r -= len ;
	return r ;
} // function


int main ( int argc , char ** argv ) {

// printf("there are %d argument(s)\n", argc);

char * outp = "" ;

// if no arguments were passed
if ( argc < 2 ) {
outp = "\n C language preprocessor\n"\
		"usage: prep /absolute/path/to/main.c optional-arguments\n\n"\
		"about: ANSI-C compliant syntax-substitution generator\n"\
		"by default, processed files are output in the directory of the main file,\n"\
		"but can be changed using the -d switch. each file generates a corresponding\n"\
		"ppc file for compiler processing, of which the processed sequence is output\n"\
		"to the file order.ppc, and can be changed to output one file using the -m switch\n"\
		"afterward no order file will be generated, and the output ppc file will be\n"\
		"labeled the same as the main input file. a library path can be specified using\n"\
		"the -l switch for includes that use <braces> rather than relative files.\n"\
		"all directory paths passed need to be the absolute, canonical, path.\n";
} else {

outp = "\n done." ;

// char * args = listargs ( argc , argv ) ;

deque * files = NULL ;
// reference the last argument
unsigned char * fname = argv[argc-1] ;
unsigned char * master_dir = NULL ;
unsigned char * lib_dir = "" ;
//somewhere to store a pointer
void * fbuff;
unsigned int flen = 0 ;
lblvect * fpkg = loc ( sizeof ( lblvect ) ) ;

unsigned char dirgraph [] = { FSLASH , EOS } ;
unsigned char extgraph [] = { '.' , 'p' ,'p' , 'c' , EOS } ;

//TODO: pass a PATH specifier if one is provided as a program argument
//prep_init ("/dir/") ;
prep_init (lib_dir) ;

while ( fname != NULL ){
	// retrieve the length of the file while storing it in a buffer
	flen = fload ( &fbuff , 0 , fname ) ;

	if ( flen == 0 ) {
		fputs( "notice, file empty or unreadable: " ,stdout);
		fputs( fname ,stdout);
		fputs( "\n" ,stdout);
		// get the next filename from the inclusion list
		prep_fnext ( &fname ) ;
	} else {
		// seek past parent directories of the file name
		unsigned char * fid = strlocate ( fname , dirgraph ) ;
		if ( * fid == FSLASH ){
			while ( * fid == FSLASH ){
				unsigned char * nxtslash = strlocate ( fid+1, dirgraph ) ;
				if ( * nxtslash == FSLASH ) {
					fid = nxtslash ;
				} else fid += 1 ;
			}
		} else fid = fname ;

		// save the master directory
		if ( master_dir == NULL ){
			unsigned int dirlen = 2 ;
			unsigned char * dirptr = "./" ;
			if( fid != fname ) {
				dirlen = 0 ;
				dirptr = fname ;
				while ( dirptr != fid ) { dirptr += 1 ; dirlen += 1 ; }
				dirptr = fname ;
			}
			master_dir = loc ( dirlen + 1 ) ;
			MEMCOPY ( master_dir , fname , dirlen )
			master_dir [dirlen] = EOS ;
		}
		// find the last extension if one exists
		unsigned char * fext = streos ( fid ) ;
		while ( * fext != POINT  && fext != fid ) { fext -= 1 ; }
		unsigned int idlen = strlength ( fid ) ;
		if ( fext != fid ) { idlen -= strlength ( fext ) ; }
		// copy the identifier of the file name
		unsigned char * rcsid = loc ( idlen + 1 ) ;
		MEMCOPY ( rcsid , fid , idlen )
		rcsid [idlen] = EOS ;

		// add the resource id to the files queue
		deqpush ( rcsid , & files ) ;
		// shift from the newly added item
		DEQBUMP ( files )

		//begin the buffer with an ENDL so a first-line-directive can be parsed
		unsigned char * fdest = loc ( flen + 1 ) ;
		unsigned char * fsrc = fbuff ;
		* fdest = EOL ;
		fdest += 1 ;
		MEMCOPY ( fdest , fsrc , flen )
		fdest -= 1 ;
		// release the non-shifted buffer and redirect its pointer
		rel ( fbuff ) ;
		fbuff = fdest ;
		// add the extra symbol to the buffers length
		flen += 1 ;

		// remove non-code
		dcomment ( flen , fbuff ) ;
		// package the buffer with the returned length from fload, and the buffer
		fpkg -> l = flen ;
		fpkg -> v = fbuff ;
		fpkg -> c = fname ;

		prep_ifile ( fpkg ) ;

		prep_fnext ( & fname ) ;

		// release the buffer of the file only
		free ( fbuff ) ;
		if ( prep_err != EOS ) { outp = prep_err ; fname = NULL ; }
		else {
			// get the location of the processed buffer
			prep_ofile ( fpkg ) ;
			if ( fpkg != NULL ){
				// integrity check the buffer container
				if ( fpkg -> v != NULL ) {
					// create an extented resource id string of the last file
					unsigned char * fileid = extend_str ( files -> item -> data , extgraph ) ;
					// write the file buffer from prep
					unsigned int l = fstore ( fpkg -> l , fpkg -> v , extend_str ( master_dir , fileid ) ) ;
					if ( l == 0 ) {
						fname = NULL ;
						prep_err = "error writing file" ;
					} // if no bytes were written to a file
					// release the temporary id string
					rel ( fileid ) ;
				}
			}
		} // else errors were not thrown by prep, process the output buffer
	} // else the file was read and contains data
	// if another file is to be processed, and the file does not have a
	// prepended library path from parsing a <bracketed> include file
	if ( fname != NULL && * strlocate ( fname , lib_dir) == EOS ){
		//prefix the name of the next file with the master directory
		fname = extend_str ( master_dir , fname ) ;
	}

} // while

prep_fini ( ) ;
// release the pointers of the file package
rel ( fpkg ) ;

if ( prep_err == EOS ) {
	if ( deqchk ( files ) != NULL ) {
		// copy the file entries to a list string delimited by ENDL
		vect * idlist = NULL ;
		DEQFIRST ( files )
		unsigned char * fileid ;
		unsigned int l ;
		unsigned int entries = files -> length ;

		while ( entries > 0 ) {
			fileid = extend_str ( files -> item -> data , extgraph ) ;
			// overwrite the terminator from extgraph with an ENDL delimiter
			l = strlength ( fileid ) ;
			fileid [l] = EOL ;
			vect_add ( & idlist , fileid , l + 1 ) ;
			// goto the next file name to write, if any
			DEQBUMP ( files )
			rel ( fileid ) ;
			entries -= 1 ;
		} // while
		// write the order file
		fstore ( idlist -> l , idlist -> v , extend_str ( master_dir , "order.ppc" ) ) ;
		// release the concatenated string
		vect_rel ( idlist ) ;
		// deallocate the file name queue
		deq_rel ( files ) ;
	}
	// no file names were added to the 'files' queue
	else prep_err = "no files processed" ;
} // if no errors were thrown

} // else the program has arguments

// hook any errors
if ( prep_err != EOS ) {
	fputs ( "error: ", stdout ) ;
	outp = prep_err ;
}

fputs ( outp , stdout ) ;
fputs ( "\n", stdout ) ;

return 0 ;

} // main
