#include <stdio.h>
#include <stdlib.h>

#ifdef FSTORE

// returns the number of bytes successfully written
unsigned int fstore ( unsigned int l , unsigned char * b , unsigned char * s ) {

	unsigned int r = 0 ;

	// create a file in binary mode
	FILE * f = fopen ( s , "wb" ) ;
	if ( f != EOF ) {
		// write the buffer stream in bytes
		r = fwrite ( b , 1 , l , f ) ;

		fclose ( f ) ;
	} // if the file was created/ overwritten

	return r ;

} // function

#endif
