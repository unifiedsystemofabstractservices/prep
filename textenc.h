/*
* Code-point portability for Utility Services of Abstract Systems.
*/

#ifndef ASCII
#include "ascii.h"
#endif
// migrated from ctype
#ifndef CTYPE
#define CTYPE

#define	_U	0x01
#define	_L	0x02
#define	_N	0x04

#define	_S	0x08
#define	_P	0x10
#define	_C	0x20
#define	_X	0x40
#define	_B	0x80


static unsigned int isalph( unsigned int c ) {
	if ( c > 96 && c < 123 ) return 1;
	else { return 0 ; }
}

//#define	ISALPHA(c)	isalph(c)
#define	ISDIGIT(c)	( ((unsigned int) ( c ) ) & (_N) )
#define	ISALNUM(c)	( ((unsigned int) ( c ) ) & (_U|_L|_N) )

#endif
