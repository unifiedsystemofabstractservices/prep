
#ifndef NULL
#include "null.h"
#endif

#ifndef DEQUE

#define DEQUE

typedef struct link_struct {
	void * next ;
	void * prev ;
} links ;

typedef struct qent_struct {
	void * data ;
	links * link ;
	void ( * rel ) ( void * ) ;
} qent ;

typedef struct deque_struct {
	unsigned int length ;
	qent * item ;
	void ( * rel ) ( void * ) ;
} deque ;


void qent_rel ( qent * p ) {
	if ( p != NULL ) {
		if ( p -> link != NULL ) { free ( p -> link ) ; }
		free ( p ) ;
	}
} // function

qent * qent_loc ( void * i , void * p , void * n ) {

	qent * r = malloc ( sizeof ( qent ) ) ;

	r -> link = malloc ( sizeof ( links ) ) ;

	r -> link -> prev = p ;

	r -> link -> next = n ;

	r -> data = i ;

	r -> rel = & qent_rel ;

	return r ;

} // function

deque * deqchk ( deque * p ) {

	deque * r = NULL ;
	//if the list object is valid
	if ( p != NULL ) {
		// if the item object is valid
		if ( p -> item != NULL ) {
			r = p ;
		}
	}

	return r ;

} //function

#define DEQFIRST(l) if ( deqchk ( l ) != NULL ) while ( ( l ) -> item -> link -> prev != NULL ) { ( l ) -> item = ( l ) -> item -> link -> prev ; }

#define DEQLAST(l) if ( deqchk ( l ) != NULL ) while ( ( l ) -> item -> link -> next != NULL ) { ( l ) -> item = ( l ) -> item -> link -> next ; }

#define DEQBUMP(l) if ( deqchk ( l ) != NULL ) if ( ( l ) -> item -> link -> next != NULL ) { ( l ) -> item = ( l ) -> item -> link -> next ; }

#define DEQRBUMP(l) if ( deqchk ( l ) != NULL ) if ( ( l ) -> item -> link -> prev != NULL ) { ( l ) -> item = ( l ) -> item -> link -> prev ; }


void deq_rel ( deque * q ) {
	if ( q != NULL ) {
		if ( q -> item != NULL ) {
			if ( q -> item -> link != NULL ) {
				// seek to the first item
				DEQFIRST ( q )
				while ( q ->item -> link -> next != NULL ) {
					q -> item = q -> item -> link -> next ;
					qent_rel ( q -> item -> link -> prev ) ;
				} // while
			}
			// remove the last qent reference
			qent_rel ( q -> item ) ;
		}
		free ( q ) ;
	}
} // function

deque * deq_loc ( void * i ) {
	//instantiate a new list
	deque * r = malloc ( sizeof ( deque ) ) ;
	//initialize the only set variable of the type
	r -> length = 1 ;
	r -> item = qent_loc( i , NULL , NULL ) ;
	r -> rel = & deq_rel ;

	return r ;

} // function

#endif

#ifndef DEQSEEK
#define DEQSEEK
#include "DEQUE/deqseek.c"
#endif

#ifndef DEQINSERT
#define DEQINSERT
#include "DEQUE/deqinsert.c"
#endif

#ifndef DEQPUSH
#define DEQPUSH
#include "DEQUE/deqpush.c"
#endif

#ifndef DEQPOP
#define DEQPOP
#include "DEQUE/deqpop.c"
#endif
