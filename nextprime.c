/*
 * NEXTPRIME - Return the next prime number following the one provided.
 */
#ifdef NEXTPRIME

unsigned int nextprime ( unsigned int beg_val ) {

	unsigned int prime = 0 ;

	unsigned char exit = 0 ;

	while ( exit == 0 ) {

		unsigned int test_val = 2 ;

		beg_val += 1 ;

		// if the data type overflows
		if ( beg_val < test_val ) { exit = 1 ; }

		while ( test_val < beg_val) {

			unsigned int rem = beg_val % test_val ;
			// break the loop if the value divides evenly by another number
			if ( rem == 0 ) { test_val = beg_val ; }
			else if ( test_val == (beg_val-1) ) {
					prime = beg_val ;
					exit = 1 ;
			} // else no divisions have occurred by the last iteration
			test_val += 1 ;
		} // nested while

	} // while

	return prime ;

} // function

#endif
