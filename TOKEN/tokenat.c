/*
 * TOKENAT - Return a pointer to the specified phrase index (n);
 * 				modification of strtok.
 *
 * Accepts the parent string, the delimiter, and the phrase number
 *
 */

#ifdef TOKENAT

char * tokenat( char * s , char * d , unsigned int n ) {

	char * c = s ;

	if ( n > 0 ) {

	char * p = d ;

	unsigned int num = 0 ;

	unsigned int exit = 0 ;

	while ( exit == 0 ) {
		// break
		if ( * c == EOS ) { exit = 1 ; }
		else {
			if ( * c == * p ) { p += 1 ; }
			else {
				p = d ;
				if ( * c == * p ) { p += 1 ; }
			}
			// edge trigger the the next iteration
			if ( * p == EOS ) {
				p = d ;
				num += 1 ;
				if ( num == n ) { exit = 1 ; }
			}
			c += 1 ;
		} // else
	} // while

	} // if not the first index

	return c ;

} // function

#endif
