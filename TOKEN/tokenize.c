/*
 * TOKENIZE - converts the string (s) to tokens using delimiter (d)
 *
 * Returns a vector with a length equal to the number of tokens,
 * 		and a vector of pointers to tokens within (s).
 */
#ifdef TOKENIZE

vect * tokenize ( unsigned char * s , unsigned char * d ) {

	vect * r = loc ( sizeof ( vect ) ) ;

	unsigned int toknum = numtokens ( s , d ) ;

	unsigned char ** tokptr = loc ( sizeof ( char * ) * toknum ) ;

	r -> l = toknum ;

	r -> v = tokptr ;

	unsigned int t = toknum ;

	while ( t > 0 ) {
		t -= 1 ;
		tokptr [t] = strtoken ( tokenat ( s , d , t ) , d )  ;
	}

	return r ;

} // function

#endif
