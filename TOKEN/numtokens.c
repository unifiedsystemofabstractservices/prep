/*
 * NUMTOKENS - Return the number of tokens seperated by the delimiter
 *
 *Accepts a c string (vector) and the delimiter character
 *
 */

#ifdef NUMTOKENS

unsigned int numtokens( unsigned char * s , unsigned char * d ) {

	unsigned char * c = s ;

	unsigned char * p = d ;

	unsigned int num = 0 ;

	char delimend = 0 ;

	while ( * c != EOS ) {
		delimend = 0 ;
		if ( * c == * p ) { p += 1 ; }
		else {
			p = d ;
			if ( * c == * p ) { p += 1 ; }
		}
		if ( * p == EOS ) {
			num += 1 ;
			p = d ;
			delimend = 1 ;
		}
		c += 1 ;
	} // while

	if ( delimend == 0 ) { num += 1 ; }

return num ;

} // function

#endif
