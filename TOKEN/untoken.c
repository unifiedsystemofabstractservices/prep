/*
 * UNTOKEN - Reverts the tokenize operation, using the symbol (d)
 *
 * Returns a reference to the string vector of (s)
 */
#ifdef UNTOKEN

unsigned char * untoken ( vect * s , unsigned char d ) {

unsigned char * c ;

/*  The vectors length is a real number,
 *  which makes the syntax unconventional when used as a subscript.
 */
while ( s -> l > 1 ) {
	// skip the last token which terminates the string
	s -> l -= 1 ;
	c = ( ( unsigned char ** ) ( s -> v ) ) [s -> l] ;
	// backtrack to the token terminator
	c -= 1 ;
	// overwrite terminations with the delimiter (d)
	while ( * c == EOS ) { * c = d ; c -= 1 ; }
} // while

s -> l = strlength ( s -> v ) + 1 ;

return ( unsigned char * ) ( s -> v ) ;

} // function

#endif
